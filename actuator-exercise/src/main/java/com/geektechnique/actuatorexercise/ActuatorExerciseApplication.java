package com.geektechnique.actuatorexercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActuatorExerciseApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActuatorExerciseApplication.class, args);
	}

}
